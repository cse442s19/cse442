//
//  EmojiCollectionCell.swift
//  MapChat
//
//  Created by Baily Troyer on 4/26/19.
//  Copyright © 2019 CSE442Group. All rights reserved.
//

import Foundation
import UIKit

class EmojiCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var emojiIcon: UILabel!
    
}
