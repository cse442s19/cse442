//
//  LocationServicesManager.swift
//  MapChat
//
//  Created by Baily Troyer on 2/26/19.
//  Copyright © 2019 CSE442Group. All rights reserved.
//

import Foundation
import CoreLocation

class LocationServicesManager: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationServicesManager()
    
}
