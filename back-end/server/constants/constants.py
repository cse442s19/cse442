UPLOAD_PATH = 'database/uploads/'

# NEO4J_CLUSTER_IP = "localhost"
NEO4J_CLUSTER_IP = "34.66.225.189"

GCS_BUCKET_NAME = "cachr-images"

DEFAULT_PROFILE_IMAGE = "https://storage.googleapis.com/cachr-images/profile_images/DEFAULT.png"

EARTH_RADIUS_METERS = 6371000

# temporary api key for testing
PLACES_API_KEY = 'AIzaSyAYDeO-TqsstxXzmZqCes-J3exBDQgD5cY'
DEFAULT_PLACE_PHOTO = 'https://www.htmlcsscolor.com/preview/gallery/005BBB.png'

# for places tagged with the place 'Other'
OTHER_PHOTO_URL = 'https://www.htmlcsscolor.com/preview/gallery/005BBB.png'
