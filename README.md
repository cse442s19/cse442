Name Not Yet Created: 

The following project will utilize swift to create an iOS app that allows users to drop messages at their current GPS location. 

Specifically: If a user goes to a restaurant, they an drop a message in the restaurant saying what they ordered and what they liked about it.
If another user with the app walks within proximity of where the message was dropped, they get the added insight from the other user that dropped the message. 


We are hosting Atlassian Confluence & Jira on 2 separate GCP compute engines. TA's can sign in and view our project with the username and password: 
http://35.231.1.144:8080
email: ta@buffalo.edu
Full name: ta
username: ta
password: password

NOTE TO TA: Upon signing in, you will be able to select JIRA at the top left hamburger menu